﻿namespace Literals
{
    public static class Decimals
    {
        public static decimal ReturnDecimal51()
        {
            // TODO #5-1. Return "0.0" literal.
            throw new NotImplementedException();
        }

        public static decimal ReturnDecimal52()
        {
            // TODO #5-2. Return "0.0000001" literal.
            throw new NotImplementedException();
        }

        public static decimal ReturnDecimal53()
        {
            // TODO #5-3. Return "-10,000.0000000001" literal.
            throw new NotImplementedException();
        }

        public static decimal ReturnDecimal54()
        {
            // TODO #5-4. Return "1,048,294,829,438,549,029,840,452,834.109492298482" literal.
            throw new NotImplementedException();
        }

        public static decimal ReturnDecimal55()
        {
            // TODO #5-5. Return "-30,492,996,837,502,378,502,387,459,850.942692284652825" literal.
            throw new NotImplementedException();
        }

        public static decimal ReturnDecimal56()
        {
            // TODO #5-6. Return "0.6 + 0.1" expression.
            throw new NotImplementedException();
        }
    }
}
